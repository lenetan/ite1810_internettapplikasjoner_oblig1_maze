package simulator;

import mazeoblig.Box;
import mazeoblig.GameDetails;
import mazeoblig.ServerInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class VirtualUser extends UnicastRemoteObject implements UserInterface {
    private int dim;
    private Box[][] maze;
    private int totalPositionsMoved = 0;

    private int xp;
    private int yp;
    boolean found = false;
    private boolean isActiveClient;

    private PositionInMaze[] firstIteration;
    private PositionInMaze[] nextIteration;
    private PositionInMaze[] positionInMaze;

    private GameDetails gameDetails;
    private ServerInterface serverInterface;
    private Stack<PositionInMaze> myWay = new Stack<>();
    private ConcurrentHashMap<UUID, GameDetails> gameClientDetailsCollection = null;

    // Construct a new user
    public VirtualUser(Box[][] maze, ServerInterface serverInterface) throws RemoteException {
        super();
        this.maze = maze;
        this.serverInterface = serverInterface;
        this.gameDetails = new GameDetails(this);
        dim = maze[0].length;
        setClientStatus(true);
        init();
    }

    private void init() {
        try {
            // Register clientId in gameDetails if not already registered
            if (gameDetails.getClientId() == null) {
                gameDetails.setClientId(serverInterface.setClientId(this));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // Move in maze and save position
        makeFirstIteration();
        makeNextIteration();
        positionInMaze = VirtualUser.concat(firstIteration, nextIteration);
    }

    private void solveMaze() {
        found = false;
        myWay.push(new PositionInMaze(xp, yp));
        backtrack(maze[xp][yp], maze[1][0]);
    }

    private void backtrack(Box b, Box from) {
        if ((xp == dim - 2) && (yp == dim - 2)) {
            found = true;
            return;
        }
        Box[] adj = b.getAdjecent();
        for (Box box : adj) {
            if (!(box.equals(from))) {
                adjustXYBeforeBacktrack(b, box);
                myWay.push(new PositionInMaze(xp, yp));
                backtrack(box, b);
                if (!found) myWay.pop();
                adjustXYAfterBacktrack(b, box);
            }
            if (found) {
                break;
            }
        }
    }

    private void adjustXYBeforeBacktrack(Box from, Box to) {
        if ((from.getUp() != null) && (to.equals(from.getUp()))) yp--;
        if ((from.getDown() != null) && (to.equals(from.getDown()))) yp++;
        if ((from.getLeft() != null) && (to.equals(from.getLeft()))) xp--;
        if ((from.getRight() != null) && (to.equals(from.getRight()))) xp++;
    }

    private void adjustXYAfterBacktrack(Box from, Box to) {
        if ((from.getUp() != null) && (to.equals(from.getUp()))) yp++;
        if ((from.getDown() != null) && (to.equals(from.getDown()))) yp--;
        if ((from.getLeft() != null) && (to.equals(from.getLeft()))) xp++;
        if ((from.getRight() != null) && (to.equals(from.getRight()))) xp--;
    }

    private PositionInMaze[] solve() {
        solveMaze();
        PositionInMaze[] pos = new PositionInMaze[myWay.size()];
        for (int i = 0; i < myWay.size(); i++)
            pos[i] = myWay.get(i);
        return pos;
    }

    private PositionInMaze[] roundAbout() {
        PositionInMaze[] pos = new PositionInMaze[dim * 2];
        int j = 0;
        pos[j++] = new PositionInMaze(dim - 2, dim - 1);
        if (System.currentTimeMillis() % 2 == 0) {
            for (int i = dim - 1; i >= 0; i--)
                pos[j++] = new PositionInMaze(dim - 1, i);
            for (int i = dim - 1; i >= 1; i--)
                pos[j++] = new PositionInMaze(i, 0);
        } else {
            for (int i = dim - 1; i >= 1; i--)
                pos[j++] = new PositionInMaze(i, dim - 1);
            for (int i = dim - 1; i >= 0; i--)
                pos[j++] = new PositionInMaze(0, i);
        }
        return pos;
    }

    @SuppressWarnings("unused")
    private PositionInMaze[] solveFull() {
        solveMaze();
        PositionInMaze[] pos = new PositionInMaze[myWay.size()];
        for (int i = 0; i < myWay.size(); i++)
            pos[i] = myWay.get(i);
        return pos;
    }

    private void makeFirstIteration() {
        xp = 1;
        yp = 1;
        myWay = new Stack<>();
        PositionInMaze[] outOfMaze = solve();
        PositionInMaze[] backToStart = roundAbout();
        firstIteration = VirtualUser.concat(outOfMaze, backToStart);
    }

    private void makeNextIteration() {
        xp = 1;
        yp = 1;
        myWay = new Stack<>();
        PositionInMaze[] outOfMaze = solve();
        PositionInMaze[] backToStart = roundAbout();
        nextIteration = VirtualUser.concat(outOfMaze, backToStart);
    }

    private static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public void sendInfoToServer() {
        // Save next position in maze and update the clients gameDetails
        gameDetails.setPositionInMaze(positionInMaze[totalPositionsMoved]);
        setClientStatus(isActiveClient());
        totalPositionsMoved++;

        // Send client gameDetails to server
        try {
            serverInterface.sendClientGameDetailsToServer(gameDetails);
        } catch (RemoteException e) {
            System.out.println("Server not running. Try again later.");
        }

        // If client has traversed the entire maze, reset travel path
        if (totalPositionsMoved >= positionInMaze.length) {
            totalPositionsMoved = 0;
        }
    }

    @Override
    public UUID getClientId() {
        return this.gameDetails.getClientId();
    }

    @Override
    public Boolean isActiveClient() {
        return this.isActiveClient;
    }

    @Override
    public void setClientStatus(boolean activeClient) {
        this.isActiveClient = activeClient;
        this.gameDetails.setActiveClient(activeClient);
    }

    @Override
    public void receiveGameDetailsFromServer(ConcurrentHashMap<UUID, GameDetails> clientGameDetailsHashMap) {
        setClientGameDetailsCollection(clientGameDetailsHashMap);
    }

    public ConcurrentHashMap<UUID, GameDetails> getClientGameDetailsCollection() {
        return gameClientDetailsCollection;
    }

    private void setClientGameDetailsCollection(ConcurrentHashMap<UUID, GameDetails> clientGameDetailsHashMap) {
        this.gameClientDetailsCollection = clientGameDetailsHashMap;
    }
}