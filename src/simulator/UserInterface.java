package simulator;

import mazeoblig.GameDetails;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public interface UserInterface extends Remote {
    UUID getClientId() throws RemoteException;

    Boolean isActiveClient() throws RemoteException;

    void setClientStatus(boolean activeClient) throws RemoteException;

    void receiveGameDetailsFromServer(ConcurrentHashMap<UUID, GameDetails> clientGameDetailsHashMap) throws RemoteException;
}