package mazeoblig;

import simulator.UserInterface;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ServerImpl extends UnicastRemoteObject implements ServerInterface {
    static final String serviceName = "ServerConsole";
    private static ConcurrentHashMap<UUID, UserInterface> clientList = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<UUID, GameDetails> activeClientList = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<UUID, UserInterface> notActiveClientList = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<UUID, GameDetails> clientGameDetailsHashMap = new ConcurrentHashMap<>();

    ServerImpl() throws RemoteException {
    }

    // Create and set clientId
    public UUID setClientId(UserInterface userInterface) {
        synchronized (clientList) {
            UUID clientId = UUID.randomUUID();

            // Add client to clientList
            clientList.put(clientId, userInterface);
            return clientId;
        }
    }

    @Override
    public void sendClientGameDetailsToServer(GameDetails gameDetails) {
        synchronized (clientList) {
            synchronized (clientGameDetailsHashMap) {
                synchronized (activeClientList) {

                    // Register client and userInterface if it doesn't exist already
                    if (!(clientList.containsKey(gameDetails.getClientId()))) {
                        clientList.put(gameDetails.getClientId(), gameDetails.getUserInterface());
                    }

                    // Add client to activeClientList if active
                    if (gameDetails.isActiveClient()) {
                        activeClientList.put(gameDetails.getClientId(), gameDetails);
                    } else {
                        try {
                            throw new ConnectException("Client not connected");
                        } catch (NullPointerException | RemoteException e) {
                            activeClientList.remove(gameDetails.getClientId());
                            clientList.remove(gameDetails.getClientId());
                            clientGameDetailsHashMap.remove(gameDetails.getClientId());
                        }
                    }
                }

                // Store clientGameDetails on server
                clientGameDetailsHashMap.put(gameDetails.getClientId(), gameDetails);
            }
        }
    }

    @Override
    public void sendClientGameDetailsFromServer() {
        synchronized (clientList) {
            synchronized (clientGameDetailsHashMap) {
                clientList.forEach((id, client) -> {
                    try {

                        // If client not active, throw connectionException to add user to notActiveClientList
                        if (!client.isActiveClient()) {
                            throw new ConnectException("Client not connected");
                        }

                        // Send data from server to active clients
                        client.receiveGameDetailsFromServer(clientGameDetailsHashMap);

                    } catch (NullPointerException | RemoteException npe) {
                        notActiveClientList.put(id, client);
                    }
                });
                // Removes not active client from lists
                notActiveClientList.forEach((id, client) -> {
                    activeClientList.remove(id);
                    clientList.remove(id);
                    clientGameDetailsHashMap.remove(id);
                });
                notActiveClientList.clear();
            }
        }
    }

    @Override
    public ConcurrentHashMap<UUID, GameDetails> getActiveClientsList() {
        synchronized (activeClientList) {
            return activeClientList;
        }
    }
}