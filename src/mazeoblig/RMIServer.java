package mazeoblig;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServer {
    private static ServerImpl serverConsole;
    private final static String DEFAULT_HOST = "undefined";
    private static InetAddress myAdress = null;
    private final static int DEFAULT_PORT = 9000;
    private static int PORT = DEFAULT_PORT;
    private static String HOST_NAME;
    static String mazeName = "Maze";
    static String serverName = "Server";

    public RMIServer() throws RemoteException, MalformedURLException {
        getStaticInfo();

        LocateRegistry.createRegistry(PORT);
        System.out.println("RMIRegistry created on host computer " + HOST_NAME +
                " on port " + PORT);
        System.out.println("Port " + PORT + " is already in use");

        serverConsole = new ServerImpl();
        String serverConsoleUrlString = "//" + "127.0.0.1" + ":" + PORT + "/" +
                ServerImpl.serviceName;
        Naming.rebind(serverConsoleUrlString, serverConsole);
        System.out.println("Remote serverConsole created");

        BoxMaze maze = new BoxMaze(Maze.DIM);
        System.out.println("Remote implementation mazeObject created");

        String mazeUrlString = "//" + HOST_NAME + ":" + PORT + "/" +
                mazeName;
        Naming.rebind(mazeUrlString, maze);
        System.out.println("Remote serverImpl object created");

        ServerImpl serverImpl = new ServerImpl();
        String infoToServerString = "//" + HOST_NAME + ":" + PORT + "/" +
                serverName;
        Naming.rebind(infoToServerString, serverImpl);
        System.out.println("Bindings Finished, waiting for client requests.");
    }

    private static void getStaticInfo() {
        if (HOST_NAME == null) HOST_NAME = DEFAULT_HOST;
        if (PORT == 0) PORT = DEFAULT_PORT;
        if (HOST_NAME.equals("undefined")) {
            try {
                myAdress = InetAddress.getLocalHost();
                HOST_NAME = myAdress.getHostName();
            } catch (java.net.UnknownHostException e) {
                System.err.println("Klarer ikke å finne egen nettadresse");
                e.printStackTrace(System.err);
            }
        } else {
            System.out.println("En MazeServer kjører allerede, bruk den");
        }
        System.out.println("Maze server navn: " + getHostName());
        System.out.println("Maze server ip:   " + getHostIP());
    }

    static int getRMIPort() {
        return PORT;
    }

    static String getHostName() {
        return HOST_NAME;
    }

    public static String getHostIP() {
        return myAdress.getHostAddress();
    }

    public static void main(String[] args) throws Exception {
        try {
            @SuppressWarnings("unused")
            RMIServer rmi = new RMIServer();
        } catch (java.rmi.UnknownHostException uhe) {
            System.out.println("Maskinnavnet " + HOST_NAME + " er ikke korrekt.");
        } catch (RemoteException re) {
            System.out.println("Error starting service");
            System.out.println("" + re);
            re.printStackTrace(System.err);
        } catch (MalformedURLException mURLe) {
            System.out.println("Internal error" + mURLe);
        }
        System.out.println("RMIRegistry on " + HOST_NAME + ":" + PORT + "\n----------------------------");

        System.out.println("Starting Server Console");

        ServerConsole Console = new ServerConsole(serverConsole);
        Console.run();
    }
}