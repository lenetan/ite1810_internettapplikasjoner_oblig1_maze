package mazeoblig;

import simulator.PositionInMaze;
import simulator.VirtualUser;

import java.applet.Applet;
import java.awt.*;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Thread.sleep;

public class Maze extends Applet {
    // Alter by need
    private static int CLIENTS_TO_CREATE = 5;
    private static String MY_USER_NAME = "Lene";
    private static Color MY_COLOR = Color.green;
    static int DIM = 20;

    // Initialize game metadata
    private int dim = DIM;
    private int server_portnumber;
    private String server_hostname;
    private Box[][] maze;
    private Image offscreen;
    private Dimension dimension;
    private Graphics bufferGraphics;

    // ClientData
    private UUID clientId;
    private UUID myMapDrawingClientId = null;

    private ServerInterface serverInterface;
    private ConcurrentHashMap<UUID, GameDetails> gameClientDetailsHashMap = null;

    public void init() {
        // Get info used for drawing
        dimension = getSize();
        offscreen = createImage(dimension.width, dimension.height);
        bufferGraphics = offscreen.getGraphics();

        // Establish connections
        if (server_hostname == null)
            server_hostname = RMIServer.getHostName();
        if (server_portnumber == 0)
            server_portnumber = RMIServer.getRMIPort();
        try {
            java.rmi.registry.Registry registry = java.rmi.registry.LocateRegistry.
                    getRegistry(server_hostname,
                            server_portnumber);

            BoxMazeInterface boxMazeInterface = (BoxMazeInterface) registry.lookup(RMIServer.mazeName);
            maze = boxMazeInterface.getMaze();
            serverInterface = (ServerInterface) registry.lookup(RMIServer.serverName);

        } catch (ConnectException ce) {
            System.out.println("Can't reach server. Try again later.");
            System.exit(0);
        } catch (NullPointerException | RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // Initialize new clients and mapUpdater
    public void start() {
        for (int i = 0; i < CLIENTS_TO_CREATE; i++) {
            try {
                new CreateClient().start();
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        new RequestMapUpdate().start();
    }

    // Paint applet content
    public void paint(Graphics g) {
        // Prepare for new maze
        bufferGraphics.clearRect(0, 0, dimension.width, dimension.height);
        bufferGraphics.setColor(Color.BLACK);

        // Render new maze
        int x, y;
        for (x = 1; x < (dim - 1); ++x)
            for (y = 1; y < (dim - 1); ++y) {
                if (maze[x][y].getUp() == null)
                    bufferGraphics.drawLine(x * 10, y * 10, x * 10 + 10, y * 10);
                if (maze[x][y].getDown() == null)
                    bufferGraphics.drawLine(x * 10, y * 10 + 10, x * 10 + 10, y * 10 + 10);
                if (maze[x][y].getLeft() == null)
                    bufferGraphics.drawLine(x * 10, y * 10, x * 10, y * 10 + 10);
                if (maze[x][y].getRight() == null)
                    bufferGraphics.drawLine(x * 10 + 10, y * 10, x * 10 + 10, y * 10 + 10);
            }

        if (gameClientDetailsHashMap != null) {

            // Render clients
            gameClientDetailsHashMap.forEach((key, value) -> {
                PositionInMaze pos = value.getPositionInMaze();
                bufferGraphics.setColor(Color.BLACK);
                bufferGraphics.fillOval(pos.getXpos() * 10, pos.getYpos() * 10, 10, 10);
            });

            // Paint myClient last due to overlapping
            gameClientDetailsHashMap.forEach((key, value) -> {
                PositionInMaze pos = value.getPositionInMaze();
                if (clientId.equals(value.getClientId())) {
                    bufferGraphics.setColor(MY_COLOR);
                    bufferGraphics.fillOval(pos.getXpos() * 10, pos.getYpos() * 10, 10, 10);
                }
            });
            g.setColor(Color.BLACK);
            g.drawImage(offscreen, 0, 0, this);
        }
    }

    private class CreateClient extends Thread {

        CreateClient() {
        }

        public void run() {
            try {
                // Initialize new client
                VirtualUser user = new VirtualUser(maze, serverInterface);

                // Set clientId
                if (clientId == null) {
                    clientId = user.getClientId();
                    System.out.println("You are logged in as " + MY_USER_NAME);
                }

                // Assign a random mapdrawerclient.
                // If only one client on a device, you are the mapDrawer yourself
                if (myMapDrawingClientId == null) {
                    myMapDrawingClientId = clientId;
                }

                // Set user to not active when disconnecting and send last info to server
                Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                    user.setClientStatus(false);
                    user.sendInfoToServer();
                    System.out.println("Disconnected");
                }));

                while (true) {
                    // If client is active send its info to server
                    user.sendInfoToServer();

                    // mapDrawingClient paint changes
                    if (user.getClientId().equals(myMapDrawingClientId)) {
                        gameClientDetailsHashMap = user.getClientGameDetailsCollection();
                        repaint();
                    }
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (ArrayIndexOutOfBoundsException | NullPointerException | RemoteException aioobe) {
                System.out.println("Can't reach server. Try again later.");
                System.exit(0);
            }
        }
    }

    // Get info from server to draw the new client position
    private class RequestMapUpdate extends Thread {
        public void run() {
            try {
                while (true) {
                    sleep(500);
                    serverInterface.sendClientGameDetailsFromServer();
                }
            } catch (RemoteException | InterruptedException e) {
                System.out.println("Can't reach server. Try again later.");
                System.exit(0);
            }
        }
    }
}