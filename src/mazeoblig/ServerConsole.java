package mazeoblig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

class ServerConsole {
    private ServerImpl server;
    private static ConcurrentHashMap<UUID, GameDetails> serverActiveClientsList;

    ServerConsole(ServerImpl server) {
        this.server = server;
    }

    void run() {
        while (true) {
            menu();
            doChoice(readChoice());
        }
    }

    private void doChoice(int choice) {
        switch (choice) {
            case 1:
                listAllClients();
                break;
            case 9:
                doQuit();
                break;
            default:
                System.out.println("Not a valid choice");
                break;
        }
    }

    private void menu() {
        System.out.println("\n\n***************** Server console *****************\n");
        System.out.println("1. List all clients");
        System.out.println("9. Shut down - Terminates all connections");
        System.out.print("\n");
        System.out.print("Choice: ");
    }

    private void doQuit() {
        System.out.println("\nShutting down...\n");
        System.exit(0);
    }

    private String readString() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            return in.readLine();
        } catch (IOException ioe) {
            return null;
        }
    }

    private String readNumber() {
        String sTall = readString();
        try {
            return sTall;
        } catch (Exception e) {
            return null;
        }
    }

    private int readChoice() {
        String x = readNumber();
        if (x != null)
            try {
                return Integer.parseInt(x);
            } catch (Exception e) {
                return -1;
            }
        return -2;
    }

    // Get active clients and write info to console
    private void listAllClients() {
        serverActiveClientsList = server.getActiveClientsList();
        System.out.println("Active clients: ");
        if (serverActiveClientsList.isEmpty()) {
            System.out.println("-No active clients-");
        } else {
            serverActiveClientsList.forEach((id, client) -> {
                System.out.println("UserId: " + client.getClientId());
            });
        }
    }
}