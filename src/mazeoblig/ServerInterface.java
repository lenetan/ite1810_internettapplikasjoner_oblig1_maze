package mazeoblig;

import simulator.UserInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public interface ServerInterface extends Remote {
    UUID setClientId(UserInterface userInterface) throws RemoteException;

    void sendClientGameDetailsToServer(GameDetails gameDetails) throws RemoteException;

    void sendClientGameDetailsFromServer() throws RemoteException;

    ConcurrentHashMap<UUID, GameDetails> getActiveClientsList() throws RemoteException;
}