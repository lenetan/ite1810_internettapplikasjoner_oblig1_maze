package mazeoblig;

import simulator.PositionInMaze;
import simulator.UserInterface;

import java.io.Serializable;
import java.util.UUID;

// Contains all data about a client that will be sent between client and server.
// It is updated with relevant client info, then sent to server.
// The server retrieves the data, add its own data, and stores it in a hashmap.
// Then the hashmap with gamedetails from all clients is then sent back to each client.

public class GameDetails implements Serializable {
    private UUID clientId;
    private boolean isActiveClient;
    private PositionInMaze position;
    private UserInterface userInterface = null;

    public GameDetails(UserInterface userInterface){
        super();
    }

    PositionInMaze getPositionInMaze() {
        return position;
    }

    public void setPositionInMaze(PositionInMaze position) {
        this.position = position;
    }

    public UUID getClientId() {
        return clientId;
    }

    public void setClientId(UUID clientId) {
        this.clientId = clientId;
    }

    boolean isActiveClient() {
        return isActiveClient;
    }

    public void setActiveClient(boolean isActiveClient) {
        this.isActiveClient = isActiveClient;
    }

    UserInterface getUserInterface() {
        return userInterface;
    }
}